use XPOS_314

create table TblCategory(
Id int primary key identity(1,1),
NameCategory varchar(50) not null,
Description varchar(150),
IsDelete bit,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime
)

create table TblVariant(
Id int primary key identity(1,1),
IdCategory int not null,
NameVariant varchar(150) not null,
Description varchar(150) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

insert into TblVariant values(
	1,'Sprite','Drink of sprite',0,1,getdate(),null,null
)
select * from TblVariant

select * from tblcategory

drop table TblOrderDetail

	
create table TblProduct(
Id int primary key identity(1,1) not null,
IdVariant int not null,
NameProduct varchar(100) not null,
Price decimal(18,0) not null,
Stock int not null,
Image varchar(255) null,
IsDelete bit,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime
)

insert into TblProduct values(
	10, 'Ice Cream Gelato', 30000, 100, 'ice_cream_gelato.jpg', 0,1, getdate(),null,null
)

update TblProduct set Image = 'bread_garlic.jpeg' where Id = 2

select * from TblProduct
select * from TblVariant

create table TblOrderHeader(
	Id int Primary Key Identity(1,1),
	CodeTransaction nvarchar(20) not null,
	IdCustomer int not null,
	Amount decimal(18,2) not null,
	TotalQty int not null,
	IsCheckout bit not null,
	IsDelete bit null,
	CreateBy int not null,
	CreateDate datetime not null,
	UpdateBy int not null,
	UpdateDate datetime null,
)

create table TblOrderDetail(
	Id int Primary Key Identity(1,1),
	IdHeader int not null,
	IdProduct int not null,
	Qty int not null,
	SumPrice decimal(18,2) not null,
	IsDelete bit null,
	CreateBy int not null,
	CreateDate datetime not null,
	UpdateBy int not null,
	UpdateDate datetime null,
)

create table TblRole(
	Id int Primary Key Identity(1,1) not null,
	RoleName varchar(80) null,
	IsDelete bit null,
	CreatedBy int null,
	CreatedDate datetime not null,
	UpdatedBy int null,
	UpdatedDate datetime null,
)

insert into TblRole values(
	'Administrator',0,1,getdate(),null,null
)

insert into TblCustomer values(
	'Muafa','muafaAbdullah@gmail.com','12345678','Bogor','08123456789',1,0,1,getdate(),null,null
)

create table TblCustomer(
	Id int Primary Key Identity(1,1) not null,
	NameCustomer nvarchar(50) not null,
	Email nvarchar(50) not null,
	[Password] nvarchar(50) not null,
	Address nvarchar(max) not null,
	Phone nvarchar(15) not null,
	IdRole int null,	
	IsDelete bit not null,
	CreateBy int not null,
	CreateDate datetime not null,
	UpdateBy int null,
	UpdateDate datetime null,
)

select * from TblCustomer
select * from TblRole

select c.Id, c.NameCustomer, c.Email, c.Password , c.Address,c.Phone, c.IdRole, r.RoleName from TblCustomer as c
join TblRole as r on c.IdRole = r.Id
where c.IsDelete = 0

create table Produk(
	ProductId int Primary Key Identity(1,1),
	ProductCode varchar(5) not null,
	ProductName varchar(50) not null,
	Quantity int not null,
	Price decimal(18,2) not null,
	IsDelete bit null,
	CreateDate datetime not null,
	UpdateDate datetime null,
)

select * from Produk

insert into Produk values(
	'P0001','Obat Kalbe',10,1000,1,getdate(),null
)

create table Customer(
	CustId int Primary Key Identity(1,1),
	CustName varchar(50) not null,
	CustAddress varchar(100) not null,
	Gender bit not null,
	TanggalLahir datetime not null,
	IsDelete bit null,
	CreateDate datetime not null,
	UpdateDate datetime null,
)
create table Penjualan(
	SalesOrderId int Primary Key Identity(1,1),
	CustomerId int not null,
	ProductId int not null,
	DateOrder bit not null,
	Qty int not null,
	IsDelete bit null,
	CreateDate datetime not null,
	UpdateDate datetime null,
)

select * from TblCustomer
select * from tblmenu
select * from TblMenuAccess

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [varchar](80) NOT NULL,
	[MenuAction] [varchar](80) NOT NULL,
	[MenuController] [varchar](80) NOT NULL,
	[MenuIcon] [varchar](80) NULL,
	[MenuSorting] [int] NULL,
	[IsParent] [bit] NULL,
	[MenuParent] [int] NULL,
	[IsDelete] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__TblMenu__3214EC07F0499761] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblMenuAccess]    Script Date: 17/04/2023 17:07:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
insert into TblMenuAccess values(
1,1,1,0,getdate(),null,null
)
CREATE TABLE [dbo].[TblMenuAccess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[MenuId] [int] NULL,
	[IsDelete] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__TblMenuA__3214EC07221F6641] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TblMenu] ON 

INSERT [dbo].[TblMenu] ([Id], [MenuName], [MenuAction], [MenuController], [MenuIcon], [MenuSorting], [IsParent], [MenuParent], [IsDelete], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Home', N'Index', N'Home', N'home', 1, 0, 0, 0, 1, CAST(N'2022-10-30T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[TblMenu] ([Id], [MenuName], [MenuAction], [MenuController], [MenuIcon], [MenuSorting], [IsParent], [MenuParent], [IsDelete], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Category', N'Index', N'Category', N'key', 1, 0, 0, 0, 1, CAST(N'2022-10-30T00:00:00.000' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[TblMenu] OFF
GO

select * from tblmenuaccess
select * from tblmenu