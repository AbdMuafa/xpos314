﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProdukController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();

        public apiProdukController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<Produk> GetAllData()
        {
            List<Produk> dataProduk = db.Produks.ToList();
            return dataProduk;
        }

        [HttpGet("GetDataById/{productId}")]
        public Produk GetDataById(int productId)
        {
            Produk result = db.Produks.Where(a => a.ProductId == productId).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckCategoryByName/{productName}")]
        public bool CheckName(string productName)
        {
            Produk data = db.Produks.Where(a => a.ProductName == productName).FirstOrDefault()!;
            if (data != null)
            {
                return true;
            }
            return false;
        }

        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"P";
            string digit = "";
            Produk data = db.Produks.OrderByDescending(a => a.ProductCode).FirstOrDefault()!;
            if (data != null)
            {
                string codeLast = data.ProductCode;
                string[] codeSplit = codeLast.Split('P');
                int intLast = int.Parse(codeSplit[1]) + 1;
                digit = intLast.ToString("0000");
            }
            else
                digit = "0001";

            return code + digit;
        }

        [HttpPost("Save")]
        public VMResponse Save(Produk data)
        {
            data.CreateDate = DateTime.Now;
            data.ProductCode = GenerateCode();
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data succes saved";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Failed saved" + ex.Message;
            }

            return response;
        }


        [HttpPut("Edit")]
        public VMResponse Edit(Produk data)
        {
            Produk dt = db.Produks.Where(a => a.ProductId == data.ProductId).FirstOrDefault()!;

            if (dt != null)
            {
                dt.ProductName = data.ProductName;
                dt.Quantity = data.Quantity;
                dt.Price = data.Price;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    response.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = "Saved failed" + ex.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            Produk dt = db.Produks.Where(a => a.ProductId == id).FirstOrDefault()!;
            if (dt != null)
            {
                try
                {
                    db.Remove(dt);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = "Delete failed " + ex.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }

            return response;
        }
    }
}
