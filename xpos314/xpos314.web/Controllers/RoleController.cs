﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class RoleController : Controller
    {
        private readonly XPOS_314Context db;
        private readonly RoleService roleService;

        public RoleController(RoleService _roleService)
        {
            this.roleService = _roleService;
        }
        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMRole> data = await roleService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }

            return View(PaginationList<VMRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<JsonResult> CheckNameIsExist(string roleName)
        {
            bool isExist = await roleService.CheckCategoryByName(roleName);
            return Json(isExist);
        }
        public async Task<IActionResult> Create()
        {
            VMRole data = new VMRole();

            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromForm] VMRole dataParam)
        {

            VMResponse respon = await roleService.Create(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMRole data = await roleService.GetDataById(id);

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit([FromForm] VMRole dataParam)
        {

            VMResponse respon = await roleService.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await roleService.Delete(id);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> RestoreData(int id)
        {
            VMResponse respon = await roleService.Delete(id);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
    }
}
