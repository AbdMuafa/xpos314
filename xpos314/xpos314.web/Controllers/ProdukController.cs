﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class ProdukController : Controller
    {
        private ProdukService produkService;

        //untuk mengisi constructor
        public ProdukController(ProdukService _produkService)
        {
            this.produkService = _produkService;
        }
        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<Produk> data = await produkService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.ProductName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.ProductName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.ProductName).ToList();
                    break;
            }

            return View(PaginationList<Produk>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            Produk data = new Produk();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(Produk dataParam)
        {
            VMResponse respon = await produkService.Create(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string productName)
        {
            bool isExist = await produkService.CheckCategoryByName(productName);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            Produk data = await produkService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Produk dataParam)
        {
            VMResponse respon = await produkService.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Produk data = await produkService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await produkService.Delete(id);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }

        public async Task<IActionResult> Detail(int id)
        {
            Produk data = await produkService.GetDataById(id);
            return PartialView(data);
        }
    }
}
