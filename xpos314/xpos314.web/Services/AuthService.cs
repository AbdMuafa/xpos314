﻿using System.Text;
using Newtonsoft.Json;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
	public class AuthService
	{
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        //cara dapetin value dari appsetting json
        public AuthService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<VMCustomer> CheckLogin(string email, string password)
		{
            VMCustomer data = new VMCustomer();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckLogin/{email}/{password}");
            data = JsonConvert.DeserializeObject<VMCustomer>(apiResponse)!;

            return data;
        }

        public async Task<List<VMMenuAccess>> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> data = new List<VMMenuAccess>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
            data = JsonConvert.DeserializeObject<List<VMMenuAccess>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> CreateCustomer(TblCustomer dataParam)
        {
            //proses convert dari object ke string :
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiAuth/CreateCustomer", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        public async Task<bool> CheckEmailExisting(string email)
        {
            bool data = false;

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckEmailExisting/{email}");
            data = JsonConvert.DeserializeObject<bool>(apiResponse)!;

            return data;
        }
    }
}
