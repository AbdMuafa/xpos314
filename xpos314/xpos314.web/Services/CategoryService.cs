﻿using System.Text;
using Newtonsoft.Json;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CategoryService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        //cara dapetin value dari appsetting json
        public CategoryService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblCategory>> GetAllData()
        {
            //list data
            List<TblCategory> data = new List<TblCategory>();

            //cara untuk ngeget api, await karena asynchronous, kenapa getstringasync karena method menggunakan get
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCategory/GetAllData");

            data = JsonConvert.DeserializeObject<List<TblCategory>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(TblCategory dataParam)
        {
            //proses convert dari object ke string :
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json,UnicodeEncoding.UTF8,"application/json");
            var request = await client.PostAsync(RouteAPI + "apiCategory/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        
        public async Task<bool> CheckCategoryByName(string nameCategory)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/CheckCategoryByName/{nameCategory} ");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse)!;
            return isExist;
        }

        public async Task<TblCategory> GetDataById(int id)
        {
            TblCategory data = new TblCategory();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblCategory>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblCategory dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil api
            var request = await client.PutAsync(RouteAPI + "apiCategory/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCategory/Delete/{id}/{createBy}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
