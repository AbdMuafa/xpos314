﻿using System.Text;
using Newtonsoft.Json;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class ProdukService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        //cara dapetin value dari appsetting json
        public ProdukService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<Produk>> GetAllData()
        {
            //list data
            List<Produk> data = new List<Produk>();

            //cara untuk ngeget api, await karena asynchronous, kenapa getstringasync karena method menggunakan get
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiProduk/GetAllData");

            data = JsonConvert.DeserializeObject<List<Produk>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(Produk dataParam)
        {
            //proses convert dari object ke string :
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiProduk/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<bool> CheckCategoryByName(string productName)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduk/CheckCategoryByName/{productName} ");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse)!;
            return isExist;
        }

        public async Task<Produk> GetDataById(int productId)
        {
            Produk data = new Produk();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduk/GetDataById/{productId}");
            data = JsonConvert.DeserializeObject<Produk>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(Produk dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil api
            var request = await client.PutAsync(RouteAPI + "apiProduk/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiProduk/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
