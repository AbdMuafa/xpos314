﻿using System.Text;
using AutoMapper;
using Newtonsoft.Json;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class RoleService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public RoleService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMRole>> GetAllData()
        {
            //list data
            List<VMRole> data = new List<VMRole>();

            //cara untuk ngeget api, await karena asynchronous, kenapa getstringasync karena method menggunakan get
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiRole/GetAllData");

            data = JsonConvert.DeserializeObject<List<VMRole>>(apiResponse)!;

            return data;
        }
        public async Task<VMRole> GetDataById(int id)
        {
            VMRole data = new VMRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMRole>(apiResponse)!;

            return data;
        }
        public async Task<bool> CheckCategoryByName(string roleName)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/CheckCategoryByName/{roleName} ");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse)!;
            return isExist;
        }
        public async Task<VMResponse> Create(VMRole dataParam)
        {
            //proses convert dari object ke string :
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiRole/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Edit(VMRole dataParam)
        {
            //proses convert dari object ke string :
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiRole/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiRole/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        public async Task<VMResponse> RestoreData(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiRole/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
