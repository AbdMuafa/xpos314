﻿using System.Text;
using Newtonsoft.Json;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
	public class CustomerService
	{
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        //cara dapetin value dari appsetting json
        public CustomerService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<VMCustomer>> GetAllData()
        {
            //list data
            List<VMCustomer> data = new List<VMCustomer>();

            //cara untuk ngeget api, await karena asynchronous, kenapa getstringasync karena method menggunakan get
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");

            data = JsonConvert.DeserializeObject<List<VMCustomer>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(VMCustomer dataParam)
        {
            //proses convert dari object ke string :
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<bool> CheckCategoryByName(string nameCustomer)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/CheckCategoryByName/{nameCustomer} ");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse)!;
            return isExist;
        }

        public async Task<VMCustomer> GetDataById(int id)
        {
            VMCustomer data = new VMCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMCustomer>(apiResponse)!;

            return data;
        }

        public async Task<List<VMCustomer>> GetDataByIdRole(int id)
        {
            List<VMCustomer> data = new List<VMCustomer>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apicustomer/GetDataByIdRole/{id}");
            data = JsonConvert.DeserializeObject<List<VMCustomer>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(VMCustomer dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil api
            var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
