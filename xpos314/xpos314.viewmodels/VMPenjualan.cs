﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
    public class VMPenjualan
    {
        public int SalesOrderId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public bool DateOrder { get; set; }
        public int Qty { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
