﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
    public class VMRole
    {
        
        public int Id { get; set; }
        
        public string? RoleName { get; set; }
        public bool? IsDelete { get; set; }
        public int? CreatedBy { get; set; }
        
        public DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        
        public DateTime? UpdatedDate { get; set; }
    }
}
