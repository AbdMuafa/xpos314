﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("Customer")]
    public partial class Customer
    {
        [Key]
        public int CustId { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string CustName { get; set; } = null!;
        [StringLength(100)]
        [Unicode(false)]
        public string CustAddress { get; set; } = null!;
        public bool Gender { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TanggalLahir { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
