﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("Produk")]
    public partial class Produk
    {
        [Key]
        public int ProductId { get; set; }
        [StringLength(5)]
        [Unicode(false)]
        public string ProductCode { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string ProductName { get; set; } = null!;
        public int Quantity { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal Price { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
