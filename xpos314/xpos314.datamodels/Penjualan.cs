﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("Penjualan")]
    public partial class Penjualan
    {
        [Key]
        public int SalesOrderId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public bool DateOrder { get; set; }
        public int Qty { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
